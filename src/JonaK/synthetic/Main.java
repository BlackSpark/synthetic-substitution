package JonaK.synthetic;

import java.util.InputMismatchException;
import java.util.Scanner;

import static JonaK.synthetic.utility.*;

/**
 *
 * @author JonaK
 */
        public class Main {


            public static void main(String[] args) {

                //Establish initial calculation parameters

                Scanner in = new Scanner(System.in);
                out("Please enter the highest integer power in your equation:");
                int hipow = in.nextInt();
                int i = 0;
                double numbers[] = new double[hipow + 1];
                do {
                    int pow = hipow - i;
                    out("Please enter the coefficient x to the power of " + pow);
                    numbers[i] = in.nextDouble();
                    i++;
                }while (i <= hipow);

                //Initial calculation

                dout(calculate(numbers, i, 0));

                //Sleep for 1 second; less clutter

                try {
                    Thread.sleep(1000);
                } catch(InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }

                //Print out quit information

                out("");
                out("You may now enter any other X value using the current synthetic input numbers.");
                out("To quit, type in any non-number character when you are asked for an X value");
                out("");

                //Sleep for 1 second; less clutter

                try {
                    Thread.sleep(1000);
                } catch(InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }

                //Calculate with a helpful tooltip once

                dout(calculate(numbers, i, 1));


                //Indefinite calculations using a dash separator

                while(true){
                    dout(calculate(numbers, i, 2));
                }
            }

            public static double calculate(double num[], int i, int time) {

                //Initialize the scanner
                Scanner in = new Scanner(System.in);
                i--;

                //Establish what help to print out, if any
                if (time == 0)
                    out("Please enter what x is equivalent to:");
                else if (time == 1)
                    out("Enter X value or 'q' for quit:");
                else if (time == 2)
                    out("-----------------------------------------------");
                else
                    out("Please enter what x is equivalent to:");

                double x;

                //Quit method

                try {
                    x = in.nextDouble();
                } catch (InputMismatchException e) {
                    x = 0;
                    out("Exiting...");
                    System.exit(0);
                }
                double y;
                double z = x;
                int ii = 0;

                //Calculation occurs here
                while (ii < i) {
                    if (ii == 0) {
                        y = num[0] * x;
                    } else {
                        y = z * x;
                    }
                    ii++;
                    z = num[ii] + y;
                }

                //Return the answer
                return z;
            }


        }

class utility {
    public static void out(String out) {
        System.out.println(out);
    }
    public static void dout (double out) {
	System.out.println(out);
    }
}